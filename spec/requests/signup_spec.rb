require 'rails_helper'

RSpec.describe 'Signup', type: :request do
  let(:url) { '/signup' }
  let(:params) do
    {
        user: attributes_for(:user)
    }
  end

  context 'when user is unauthenticated' do
    before { post url, params: params }

    it 'returns 200' do
      expect(response.status).to eq 200
    end

    it 'returns a new user' do
      expect(json.keys).to include("id", "email")
    end
    it 'returns id instead of _id' do
      expect(json["id"]).to be
    end
    it 'returns auth token' do
      expect(response.headers["Authorization"]).to be
    end
  end

  context 'when user already exists' do
    before do
      create :user, email: params[:user][:email]
      post url, params: params
    end

    it 'returns bad request status' do
      expect(response.status).to eq 400
    end

    it 'returns validation errors' do
      expect(json['errors'].first['title']).to eq('Bad Request')
    end
  end

end