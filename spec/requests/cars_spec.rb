require 'rails_helper'

RSpec.describe 'Cars search', type: :request do
  let(:url) { '/cars/search' }
  let(:user) { create(:user) }
  let(:car) { create(:car) }
  let(:params) do
    {
        filter: {dates:{
            starts_at: (Time.now).to_i,
            ends_at: (Time.now+3.days).to_i
        }}
    }
  end
  before {car} # hack mongo

  context 'all period is empty' do
    before { post url, params: params, headers:{ 'Authorization' => "Bearer #{user.jwt_token}"} }
    it 'should return list' do
      expect(json).to be_a(Array)
    end
    it 'should return 1 car' do
      expect(json.size).to eq(1)
    end
    it 'should return car' do
      expect(json.first["id"]).to eq(car.id.to_s)
    end
  end

  context 'no period returns nothing' do
    before { post url, params: {}, headers:{ 'Authorization' => "Bearer #{user.jwt_token}"} }
    it 'should return list' do
      expect(json).to be_a(Array)
    end
    it 'should return 1 car' do
      expect(json.size).to eq(0)
    end
  end

  context 'when session jwt is incorrect' do
    before { post url, params: params }
    it 'returns unathorized status' do
      expect(response.status).to eq 401
    end
    it 'should return 0 cars' do
      expect(json).to eq(nil)
    end
  end

  context 'dates overflow with booking' do
    let(:booking){create(:booking)}
    before {booking} # hack mongo
    before { post url, params: params, headers:{ 'Authorization' => "Bearer #{user.jwt_token}"} }
    it 'should return list' do
      expect(json).to be_a(Array)
    end

    # 1 car is without booking and
    # 1 car with booking
    it 'should return 1 car' do
      expect(json.size).to eq(1)
    end
  end
end