require 'rails_helper'

RSpec.describe 'Login', type: :request do
  let(:user) { create(:user) }
  let(:url) { '/login' }
  let(:params) do
    {
        user: {
            email: user.email,
            first_name: user.first_name,
            last_name: user.last_name,
            password: user.password,
        }
    }
  end

  context 'when params are correct' do
    before do
      post url, params: params
    end

    it 'returns 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns JTW token in authorization header' do
      expect(response.headers['Authorization']).to be_present
    end

    it 'returns valid JWT token' do
      user = User.find_by_token(response.headers['Authorization'])
      expect(json["id"]).to eql(user.id.to_s)
    end

    context 'track logins attempts' do
      let(:user_login){UserLogin.where(email: user.email).first}
      it 'returns unathorized status' do
        expect(response.status).to eq 200
      end
      it 'still should have attempt' do
        expect(UserLogin.where(email: user.email).count).to eql(1)
      end
      it 'should have ip set' do
        expect(user_login.remote_ip).to eql("127.0.0.1")
      end
      it 'should have user_id set' do
        expect(user_login.user_id.to_s).to eql(json["id"])
      end
    end
  end

  context 'when login params are incorrect' do
    before { post url }

    it 'returns unathorized status' do
      expect(response.status).to eq 401
    end

    context 'track logins attempts' do
      let(:invalid_params){
        new_param = params.deep_dup
        new_param[:user][:email] = "something@hot.ee"
        new_param
      }
      before do
        post url, params: invalid_params
      end
      it 'returns unathorized status' do
        expect(response.status).to eq 401
      end
      it 'still should have attempt' do
        expect(UserLogin.where(email: "something@hot.ee").count).to eql(1)
      end
      it 'should have ip set' do
        expect(UserLogin.where(email: "something@hot.ee").first.remote_ip).to eql("127.0.0.1")
      end
    end
  end
end

RSpec.describe 'Logout', type: :request do
  let(:url) { '/logout' }

  it 'returns 204, no content' do
    delete url
    expect(response).to have_http_status(204)
  end
end

RSpec.describe 'Get User info', type: :request do
  let(:user) { create(:user) }
  let(:url) { '/user' }

  context 'when session jwt is incorrect' do
    context 'no session jwt is set' do
      before { get url }
      it 'returns unathorized status' do
        expect(response.status).to eq 401
      end
    end
    context 'session jwt is set' do
      before { get url, headers:{ 'Authorization' => "Bearer 000"} }
      it 'returns unathorized status' do
        expect(response.status).to eq 401
      end
    end
  end
  context 'when session jwt is correct' do
    before { get url, headers:{ 'Authorization' => "Bearer #{user.jwt_token}"} }
    it 'returns 200' do
      expect(response).to have_http_status(200)
    end
    it 'should have user id' do
      expect(json["id"]).to eql(user.id.to_s)
    end
  end

end
