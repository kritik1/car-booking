require 'factory_bot'
FactoryBot.define do
  sequence :email do |n|
    "email#{n}@email.com"
  end

  sequence :number do |n|
    "5#{n}#{n}NYP"
  end

  sequence :price do |n|
    1.1*n
  end
end

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end