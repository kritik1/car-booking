FactoryBot.define do
  factory :car_location do
    name { "Tallinn airport" }
    lng { "9.99" }
    lat { "9.99" }
  end
end
