FactoryBot.define do
  factory :user do
    first_name {'Lev'}
    last_name {'Tolstoi'}
    email { generate :email }
    password {"password"}
  end
end
