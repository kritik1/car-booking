FactoryBot.define do
  factory :car do
    number { generate :number }
    car_location
    doors { 5 }
    seats { 5 }
    price { generate :price }
  end
end
