FactoryBot.define do
  factory :booking do
    car
    user{create(:user, aasm_state: :verified)}
    starts_at { DateTime.now }
    ends_at { DateTime.now + 5.days }
    days { 1 }
    real_starts_at { nil }
    real_ends_at { nil }
    real_days { 1 }
    total { 1.5 }
    penalty { nil }
  end
end
