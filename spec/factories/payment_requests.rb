FactoryBot.define do
  factory :payment_request do
    booking
    payment_method
    method_name { payment_method.name }
    amount { 1.5 }
    data { {} }
  end
end
