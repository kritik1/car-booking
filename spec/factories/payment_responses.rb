FactoryBot.define do
  factory :payment_response do
    booking
    payment_method
    payment_request{create(:payment_request)}
    method_name { "MyString" }
    params { "MyString" }
    amount { 1.5 }
  end
end
