FactoryBot.define do
  factory :payment_method do
    name { "Cash" }
    style { "Offline" }
  end
end
