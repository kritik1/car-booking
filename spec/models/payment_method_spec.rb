require 'rails_helper'

RSpec.describe PaymentMethod, type: :model do
  context 'check validations' do
    let(:empty_user){o=PaymentMethod.new;o.valid?; o}
    before{empty_user.valid?}

    it 'should ask for name' do
      expect(empty_user.errors[:name].presence).to be
    end
    it 'should have style automatically' do
      expect(empty_user.errors[:style].presence).to be_nil
    end
  end
  context 'configuration' do
    context 'style is offline' do
      let(:meth){ create(:payment_method) }
      it 'shouldnt have redirect_url set' do
        expect(meth.redirect_url).to be_nil
      end
      it 'should be auto_applied' do
        expect(meth.auto_apply).to be_truthy
      end
    end
  end
end
