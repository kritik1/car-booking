require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user){create(:user)}
  it 'user is unverified' do
    expect(user.aasm_state).to eq('unverified')
  end
  context "verify data if code is success" do
    before { user.update(veriff_data: {"id" => "a4c0a230-859a-4861-b99b-ac39e4a05bfd"})}
    before { Veriff.update_user_status({verification: {id:"a4c0a230-859a-4861-b99b-ac39e4a05bfd",code:9001}}.with_indifferent_access)}
    before { user.reload }
    it 'user is verified' do
      expect(user.aasm_state).to eq('verified')
    end
  end

  # cannot test as should use different veriff token
  # will uncomment once token is the same
  # context "precreate veriff session" do
  #   let(:veriff){ Veriff.new(user)}
  #   before{ veriff.start_verification;user.reload }
  #   it 'user should have token set' do
  #     expect(user.veriff_data["url"]).to be
  #   end
  #   it 'user should have token status' do
  #     expect(user.veriff_data["status"]).to eql("created")
  #   end
  # end

end
