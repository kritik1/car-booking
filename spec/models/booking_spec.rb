require 'rails_helper'

RSpec.describe Booking, type: :model do
  context 'check basic validations' do
    let(:empty_booking){o=Booking.new;o.valid?; o}
    it 'should ask for car' do
      expect(empty_booking.errors[:car].presence).to be
    end
    it 'should ask for user' do
      expect(empty_booking.errors[:user].presence).to be
    end
    it 'should have token set' do
      expect(empty_booking.token).to be
      expect(empty_booking.errors[:token].presence).to be_nil
    end
    it 'should ask for user is verified' do
      empty_booking.user = build(:user, aasm_state: "unverified")
      expect(empty_booking.errors[:user].presence).to be
    end
    it 'should ask for starts_at' do
      expect(empty_booking.errors[:starts_at].presence).to be
    end
    it 'should ask for ends_at' do
      expect(empty_booking.errors[:ends_at].presence).to be
    end
    it 'should ask for days bigger than 1' do
      empty_booking.starts_at = Time.now
      empty_booking.ends_at = Time.now
      empty_booking.valid?
      expect(empty_booking.days).to be
      expect(empty_booking.errors[:days].presence).to be
    end
    it 'should ask for days' do
      expect(empty_booking.errors[:days].presence).to be
    end
    it 'should ask for total' do
      expect(empty_booking.errors[:total].presence).to be
    end
    it 'shouldn\'t ask for real data if it is not set' do
      expect(empty_booking.real_starts_at).to be_nil
      expect(empty_booking.real_ends_at).to be_nil
      expect(empty_booking.errors[:real_starts_at].presence).to be_nil
    end

    context 'real return validation' do
      it 'should ask for real_starts_at if real_ends_at set' do
        empty_booking.real_ends_at=Time.now
        expect(empty_booking.real_starts_at).to be_nil
        empty_booking.valid?
        expect(empty_booking.errors[:real_starts_at].presence).to be
      end
      it 'should ask for real_ends_at if real_starts_at set' do
        empty_booking.real_starts_at=Time.now
        expect(empty_booking.real_ends_at).to be_nil
        empty_booking.valid?
        expect(empty_booking.errors[:real_ends_at].presence).to be
      end
      it 'should ask for real_days date set' do
        empty_booking.real_starts_at=Time.now
        expect(empty_booking.real_days).to be_nil
        empty_booking.valid?
        expect(empty_booking.errors[:real_days].presence).to be
      end

      it 'should ask for real_days is bigger than 1' do
        empty_booking.real_starts_at=Time.now
        empty_booking.real_ends_at=Time.now
        empty_booking.valid?
        expect(empty_booking.real_days).to be
        expect(empty_booking.errors[:real_days].presence).to be
      end
    end
  end

  context 'calculators' do
    let(:booking){create(:booking)}
    context "days" do
      it 'calcs days by days difference' do
        booking.starts_at = Time.now + 1.day
        booking.ends_at   = booking.starts_at + 5.days
        booking.valid?
        expect(booking.days).to eql(5)
      end
      it 'calcs days by days and hours difference' do
        booking.starts_at = Time.now + 1.day
        booking.ends_at   = booking.starts_at + 5.days + 1.hour
        booking.valid?
        expect(booking.days).to eql(6)
      end
      it 'calcs days by days and minutes difference' do
        booking.starts_at = Time.now + 1.day
        booking.ends_at   = booking.starts_at + 5.days + 1.minute
        booking.valid?
        expect(booking.days).to eql(6)
      end
      it 'calcs days by days and seconds difference' do
        booking.starts_at = Time.now + 1.day
        booking.ends_at   = booking.starts_at + 5.days + 50.seconds
        booking.valid?
        expect(booking.days).to eql(6)
      end
    end
    context "real days" do
      it 'calcs real_days by days difference' do
        booking.real_starts_at = Time.now + 1.day
        booking.real_ends_at   = booking.real_starts_at + 5.days
        booking.valid?
        expect(booking.real_days).to eql(5)
      end

      it 'calcs real_days by hour difference' do
        booking.real_starts_at = Time.now + 1.day
        booking.real_ends_at   = booking.real_starts_at + 5.days + 1.hour
        booking.valid?
        expect(booking.real_days).to eql(5)
      end
      it 'calcs real_days by hour difference' do
        booking.real_starts_at = Time.now + 1.day
        booking.real_ends_at   = booking.real_starts_at + 5.days + 1.hour + 1.second
        booking.valid?
        expect(booking.real_days).to eql(6)
      end
      it 'calcs real_days by less than hour difference' do
        booking.real_starts_at = Time.now + 1.day
        booking.real_ends_at   = booking.real_starts_at + 5.days + 1.hour - 1.second
        booking.valid?
        expect(booking.real_days).to eql(5)
      end
    end

    context "total" do
      it "calcs total based on days" do
        booking.starts_at = Time.now + 1.day
        booking.ends_at   = booking.starts_at + 5.days
        booking.valid?
        expect(booking.total).to eql(booking.car.price * 5)
      end
    end
  end

  context "scope in range period filter" do
    let(:booking_starts_at){ DateTime.new(2019,01,03)}
    let(:booking_ends_at){ DateTime.new(2019,01,13)}
    let(:booking){create(:booking, starts_at: booking_starts_at, ends_at: booking_ends_at)}
    before{booking} #make real write to db
    it 'period is before start booking date' do
      size = Booking.in_range(DateTime.new(2019,01,01), DateTime.new(2019,01,02)).count
      expect(size).to eq(0)
    end
    it 'period covering start date' do
      size = Booking.in_range(DateTime.new(2019,01,01), DateTime.new(2019,01,04)).count
      expect(size).to eq(1)
    end
    it 'period is inside booking date' do
      size = Booking.in_range(DateTime.new(2019,01,04), DateTime.new(2019,01,07)).count
      expect(size).to eq(1)
    end
    it 'period is covering end date' do
      size = Booking.in_range(DateTime.new(2019,01,10), DateTime.new(2019,01,15)).count
      expect(size).to eq(1)
    end
    it 'period is after end date' do
      size = Booking.in_range(DateTime.new(2019,01,15), DateTime.new(2019,01,20)).count
      expect(size).to eq(0)
    end
    it 'period is outer of booking' do
      size = Booking.in_range(DateTime.new(2019,01,01), DateTime.new(2019,01,15)).count
      expect(size).to eq(1)
    end
  end
end
