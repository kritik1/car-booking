require 'rails_helper'

RSpec.describe PaymentResponse, type: :model do
  let(:pay_resp) { create(:payment_response) }
  context 'check validations' do
    let(:pay_resp){o=PaymentResponse.new;o.valid?; o}
    before{pay_resp.valid?}

    it 'should ask for booking' do
      expect(pay_resp.errors[:booking].presence).to be
    end
    it 'should ask for payment_method' do
      expect(pay_resp.errors[:payment_method].presence).to be
    end
    it 'should ask for booking' do
      expect(pay_resp.errors[:payment_request].presence).to be
    end
  end

  context 'success logic' do
    context 'successful' do
      before{ pay_resp.amount = pay_resp.payment_request.amount }
      before{pay_resp.valid?}
      it 'amounts should be the same' do
        expect(pay_resp.amount).to eq(pay_resp.payment_request.amount)
      end
      it 'success is true' do
        expect(pay_resp.success).to be_truthy
      end
      it 'booking should be unpaid' do
        expect(pay_resp.booking.aasm_state).to eql("unpaid")
      end
      it 'booking should became paid' do
        pay_resp.finish_booking!
        pay_resp.booking.reload
        expect(pay_resp.booking.aasm_state).to eql("paid")
      end
    end
    context 'unsuccessful' do
      before{ pay_resp.amount = pay_resp.payment_request.amount + 1 }
      before{pay_resp.valid?}
      it 'amounts should be the same' do
        expect(pay_resp.amount).not_to eq(pay_resp.payment_request.amount)
      end
      it 'success is true' do
        expect(pay_resp.success).to be_falsey
      end
      it 'booking should be unpaid' do
        expect(pay_resp.booking.aasm_state).to eql("unpaid")
      end
      it 'booking should became paid' do
        pay_resp.finish_booking!
        pay_resp.booking.reload
        expect(pay_resp.booking.aasm_state).not_to eql("paid")
        expect(pay_resp.booking.aasm_state).to eql("unpaid")
      end
    end
  end
end
