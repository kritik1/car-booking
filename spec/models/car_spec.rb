require 'rails_helper'

RSpec.describe Car, type: :model do
  let(:car) { create(:car) }
  context 'check validations' do
    let(:empty_car){o=Car.new;o.valid?; o}
    it 'should ask for number' do
      expect(empty_car.errors[:number].presence).to be
    end
    it 'should ask for price' do
      expect(empty_car.errors[:price].presence).to be
    end
    it 'should ask for price' do
      expect(empty_car.errors[:car_location].presence).to be
    end
    it 'should block creating doubled car' do
      double_car = create(:car)
      double_car.number= car.number
      double_car.valid?
      expect(double_car.errors[:number].presence).to be
    end
  end

  context 'tracking changes' do
    it 'should have history line that car changed first name' do
      expect{car.update(price: 20)}.to change{car.history_tracks.count}.by(1)
    end
  end
end
