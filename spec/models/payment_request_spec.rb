require 'rails_helper'

RSpec.describe PaymentRequest, type: :model do
  let(:pay_req) { create(:payment_request) }
  context 'check validations' do
    let(:empty_pay_resp){o=PaymentRequest.new;o.valid?; o}
    it 'should ask for payment name' do
      expect(empty_pay_resp.errors[:method_name].presence).to be
    end
    it 'should ask for amount' do
      expect(empty_pay_resp.errors[:amount].presence).to be
    end
    it 'should ask for booking' do
      expect(empty_pay_resp.errors[:booking].presence).to be
    end
    it 'should ask for payment_method' do
      expect(empty_pay_resp.errors[:payment_method].presence).to be
    end
  end
  context 'logic' do
    let(:booking){create(:booking)}
    let(:attempt){PaymentRequest.create_attempt(booking, pay_req.payment_method.id)}
    it 'should have payment_name set' do
      expect(attempt.method_name).to eql(pay_req.payment_method.name)
    end
    it 'should amount be set' do
      expect(attempt.amount).to be
      expect(attempt.amount).to eql(booking.total)
    end
  end
end
