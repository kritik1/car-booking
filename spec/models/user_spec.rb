require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }

  it 'should have set state' do
    expect(user.unverified?).to be_truthy
  end

  context 'check validations' do
    let(:empty_user){o=User.new;o.valid?; o}
    before{empty_user.valid?}

    it 'should ask for email' do
      expect(empty_user.errors[:email].presence).to be
    end
    it 'should ask for first_name' do
      expect(empty_user.errors[:first_name].presence).to be
    end
    it 'should ask for last_name' do
      expect(empty_user.errors[:last_name].presence).to be
    end
    it 'should ask for password' do
      expect(empty_user.errors[:password].presence).to be
    end
    it 'shouldn\'t say anything about state not present' do
      expect(empty_user.errors[:aasm_state].presence).to be_nil
    end
  end

  context 'tracking changes' do
    it 'should have history line that user changed first name' do
      expect{user.update(first_name: "123")}.to change{user.history_tracks.count}.by(1)
    end
  end

  context 'on reload' do
    let(:loaded_user) {User.find(user.id)}
    it 'shouldn\'t have password in DB' do
      expect(loaded_user.password).to be_nil
    end
  end
end
