airport = CarLocation.first_or_create!(
     name: "Tallinn airport",
     lat: 59.4150,
     lng:24.7864,
)
center = CarLocation.first_or_create!(
      name: "Old town",
      lat: 59.43745,
      lng: 24.74377,
)
car=airport.cars.first_or_create!(
    number: "1231ASD",
    doors: 5,
    seats: 5,
    price: 10
)
car=center.cars.first_or_create!(
    number: "000ASD",
    doors: 3,
    seats: 4,
    price: 5
)

PaymentMethod.where(style: "Offline",name: "Pay with Cash").first_or_create
PaymentMethod.where(style: "Offline",name: "Pay with Check").first_or_create