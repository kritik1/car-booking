Rails.application.routes.draw do
  devise_for :users,
             path: '',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout',
                 registration: 'signup'
             },
             controllers: {
                 sessions: 'sessions',
                 registrations: 'registrations'
             }

  get "/user"=> "user#show"
  get "/user/verification"=> "user#ask_verification"
  # will use search index action clean for future usage
  # if we need to make API for admin
  get  "/cars/search" => "cars#search"
  post "/cars/search" => "cars#search"
  resources :bookings do
    patch :pay, on: :member
  end
  match "/webhooks/receive/:provider" => "webhooks#receive", via: [:get, :post]

  root to: "web#index"
end
