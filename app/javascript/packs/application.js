/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

import Vue from 'vue'
import VueResource from 'vue-resource'
import Antd from 'ant-design-vue'
import App from '../app.vue'
import 'ant-design-vue/dist/antd.css'
import {store} from "code/store.js"
import {router} from "code/routes.js"

Vue.use(Antd)
Vue.use(VueResource)

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector("#main-app")
  const app = new Vue({
    el,
    store,
    router,
    render: h => h(App)
  })

  console.log(app)
})