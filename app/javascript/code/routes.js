import Vue from 'vue'
import VueRouter from 'vue-router'

import SignUp from '../templates/signup'
import SignUpUploadData from '../templates/signup_upload_data'
import Login from '../templates/login'
import Booking from '../templates/booking'
import Payment from '../templates/payment'

Vue.use(VueRouter)

const routes = [
  {path: '/', component: Booking, name: 'root'},
  {path: '/booking', component: Booking},
  {path: '/signup', component: SignUp, name: 'signup'},
  {path: '/signup-verify', component: SignUpUploadData, name: 'signup_verify'},
  {path: '/login', component: Login, name: 'login'},
  {path: '/payment/:id', component: Payment, name: 'payment'},
]

export const router = new VueRouter({
  // mode: 'history',
  routes // short for `routes: routes`
})