import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const LOGIN = "LOGIN";
const LOGOUT = "LOGOUT";

export const store = new Vuex.Store({
  state: {
    current_user: {},
    booking: {},
  },
  mutations: {
    [LOGIN] (state, user_data) {
      state.current_user = user_data;
    },
    [LOGOUT](state) {
      state.current_user = {};
    },
    setHTTPHeaders(_){
      let token
      if (token = window.localStorage.getItem('jwt')){
        Vue.http.headers.common['Authorization'] = `Bearer ${token}`
      }
    },
    booking(state, booking){
      state.booking = booking
    }
  },
  actions: {
    signup({ commit }, creds) {
      return Vue.http.post("/signup",{user: creds}).then((resp)=> {
        window.localStorage.setItem('jwt', resp.headers.map.authorization[0]);
        commit("setHTTPHeaders")
        commit(LOGIN, resp.body);
      }).catch((resp)=>{
        console.log("catch")
        console.log(resp.body)
      })
    },
    login({ commit }, creds) {
      return Vue.http.post("/login",{user: creds}).then((resp)=> {
        window.localStorage.setItem('jwt', resp.headers.map.authorization[0]);
        commit("setHTTPHeaders")
        commit(LOGIN, resp.body);
      }).catch((resp)=>{
        console.log("catch")
        console.log(resp.body)
      })
    },
    loadSession({commit}) {
      commit("setHTTPHeaders")
      return Vue.http.get('/user').then((resp)=>{
        commit(LOGIN, resp.body)
        return resp.body
      })
    },
    logout({ commit }) {
      localStorage.removeItem("jwt");
      commit(LOGOUT);
    },
    booking({commit}, booking){
      commit("booking", booking);
    },
    loadBooking({commit}, id){
      return Vue.http.get(`/bookings/${id}`).then((resp)=>{
        commit("booking", resp.body);
        return resp.body
      })
    }
  },
  getters: {
    current_user: state => {
      return state.current_user
    },
    loggedIn: state => {
      return !!state.current_user.id
    },
    booking: state => {
      return state.booking
    }
  }
})