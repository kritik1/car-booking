class UserLogin
  include Mongoid::Document
  include Mongoid::Timestamps

  field :email,              type: String
  field :remote_ip,          type: String
  field :action,             type: String
  field :response_success,   type: Boolean

  belongs_to :user, optional: true

  def self.log!(request, user, response_success = true)
    UserLogin.create!(
            user_id: user.try(:id), # if login failed then
            email: request.params.dig(:user, :email),
            response_success: response_success,
            remote_ip: request.remote_ip,
            action: request.params[:action],
    )
  end
end
