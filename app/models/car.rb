class Car
  include Mongoid::Document
  include Mongoid::History::Trackable

  field :number, type: String
  field :doors, type: Integer
  field :seats, type: Integer
  field :price, type: Float
  belongs_to :car_location
  has_many :bookings

  track_history :modifier_field => nil,
                :modifier_field_optional => true

  validates :number, presence: true, uniqueness: true
  validates :price,  presence: true

  # idea is that we won't have too many cars in DB,
  # so we can fetch possible bookings at given range and then exclude them.
  # Right now just testing solution. Later will try new Mongo features.
  scope :available_for, ->(starts_at, ends_at) do
    Car.not_in(id: Booking.in_range(starts_at, ends_at).pluck(:car_id))
  end

  def as_json(options={})
    h = super
    h[:car_location] = car_location.as_json
    h
  end
end
