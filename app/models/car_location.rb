class CarLocation
  include Mongoid::Document
  field :name, type: String
  field :lng, type: Float
  field :lat, type: Float

  has_many :cars

  validates :name, presence: true
end
