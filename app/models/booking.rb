class Booking
  DAY_SECONDS = 60*60*24.to_f
  include Mongoid::Document
  include AASM

  field :token
  field :starts_at, type: DateTime
  field :ends_at, type: DateTime
  field :days, type: Integer
  field :total, type: Float
  field :aasm_state

  # next fields will be used on car taking and returning
  # to calculate real dates and ask for the fine if car
  # used longer. It's also good to have some statistics
  # when car taken and returned in reality to increase
  # efficiency.
  field :real_starts_at, type: DateTime
  field :real_ends_at, type: DateTime
  field :real_days, type: Integer
  field :penalty, type: Float


  belongs_to :car
  belongs_to :user

  validates :starts_at, presence: true
  validates :ends_at,   presence: true
  validates :days,      presence: true, numericality: {greater_than: 0}
  validates :total,     presence: true, numericality: {greater_than: 0.0}

  validates :real_starts_at, presence: true, if: :real_ends_at
  validates :real_ends_at, presence: true, if: :real_starts_at
  validates :real_days, presence: true, numericality: {greater_than: 0}, if: :real_dates?

  validate :val_ends_at_later
  validate :val_real_ends_at_later
  validate :val_user_verified

  before_validation {self.token ||= SecureRandom.uuid}
  before_validation :calc_days
  before_validation :calc_real_days
  before_validation :calc_total

  scope :in_range, ->(booking_start, booking_end){
    all.or(
        {starts_at: booking_start..booking_end},
        {ends_at: booking_start..booking_end},
        {:starts_at.lt => booking_start, :ends_at.gt=>booking_end}
    )
  }

  aasm do
    state :unpaid, initial: true
    state :paid
    state :returned

    event :pay do
      transitions from: [:unpaid,:paid], to: :paid
    end
    event :return do
      transitions from: [:paid, :returned], to: :returned
    end
  end

  def calc_days
    return if ends_at.nil? || starts_at.nil?
    seconds = ends_at.to_i - starts_at.to_i
    diff = seconds/DAY_SECONDS
    self.days = diff.ceil
  end

  # in return we don't care if there's 1 hour difference
  def calc_real_days
    return if real_ends_at.nil? || real_starts_at.nil?
    seconds = real_ends_at.to_i - real_starts_at.to_i - 1.hour.to_i
    diff = seconds/DAY_SECONDS
    self.real_days = [diff.ceil,days.to_i].max
  end
  def calc_total
    return if self.days.nil? || car.nil?
    self.total = days * car.price
  end


  def val_ends_at_later
    return if ends_at.nil? || starts_at.nil?
    errors.add(:ends_at,:before) if ends_at <= starts_at
  end

  def val_real_ends_at_later
    return if real_ends_at.nil? || real_starts_at.nil?
    errors.add(:real_ends_at, :before) if real_ends_at <= real_starts_at
  end
  def val_user_verified
    return if user_id.nil?
    errors.add(:user_id, "should be verified") if user.unverified?
  end

  def real_dates?
    real_ends_at.present? || real_starts_at.present?
  end

  def as_json(opts={})
    h=super
    h["status"] = aasm_state
    h["car"] = car.as_json
    h["payment_methods"] = PaymentMethod.all
    h
  end
end
