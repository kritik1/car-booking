class PaymentMethod
  include Mongoid::Document
  field :name, type: String
  field :style, type: String, default: "Offline"

  validates :name, presence: true
  validates :style, presence: true, inclusion: ["Offline"]

  def redirect_url
    "google.com" unless offline?
  end

  def auto_apply
    offline?
  end

  def offline?
    style == "Offline"
  end
end
