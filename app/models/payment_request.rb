class PaymentRequest
  include Mongoid::Document
  field :method_name, type: String # need if name has changed or payment method removed
  field :amount, type: Float # store request amount for any case and future
  field :data, type: Hash # future usage when create bank request. We'll save here response from Bank
  belongs_to :booking
  belongs_to :payment_method

  validates :method_name, presence: true
  validates :amount, presence: true

  def self.create_attempt(booking, payment_method_id)
    payment_method = PaymentMethod.find(payment_method_id)
    request = PaymentRequest.create!(
      booking_id: booking.id,
      payment_method_id: payment_method.id,
      method_name: payment_method.name,
      amount: booking.total
    )

    request
  end
end
