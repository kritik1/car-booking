class User
  include Mongoid::Document
  include Mongoid::History::Trackable
  include AASM

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :lockable, :trackable

  ## Database authenticatable
  field :email,              type: String
  field :first_name,         type: String
  field :last_name,          type: String
  field :encrypted_password, type: String

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  field :locked_at,       type: Time

  field :admin,           type: Boolean, default: false
  field :aasm_state,      type: String
  field :veriff_data,     type: Hash, default: {}

  has_many :user_logins



  track_history :modifier_field => nil, # adds "belongs_to :modifier" to track who made the change, default is :modifier, set to nil to not create modifier_field
                # :modifier_field_inverse_of => nil, # adds an ":inverse_of" option to the "belongs_to :modifier" relation, default is not set
                :modifier_field_optional => true


  validates :email,              presence: true
  validates :aasm_state,         presence: true
  validates :first_name,         presence: true
  validates :last_name,          presence: true
  validates :encrypted_password, presence: true

  aasm do
    state :unverified, :initial => true
    state :verified

    event :set_verified do
      transitions :from => [:unverified,:verified], :to => :verified
    end
  end


  def jwt_token
    exp = Time.now.to_i + 4 * 3600
    exp_payload = { data: {user_id: id.to_s}, exp: exp }

    JWT.encode exp_payload, ENV["RSA_PRIVATE"], 'HS256'
  end

  def self.find_by_token token
    decoded_token = JWT.decode token, ENV["RSA_PRIVATE"], true, { algorithm: 'HS256' }
    User.where(id: decoded_token.first["data"]["user_id"]).first
  rescue JWT::ExpiredSignature, JWT::DecodeError
    # user not found
  end

  def as_json(opts={})
    h=super
    h["status"] = aasm_state
    h["veriff_data"] = veriff_data.slice("url","status")
    h
  end
end
