class PaymentResponse
  include Mongoid::Document
  field :method_name, type: String
  field :params, type: String
  field :amount, type: Float
  field :data, type: Hash
  field :success, type: Mongoid::Boolean
  belongs_to :booking
  belongs_to :payment_method
  belongs_to :payment_request

  before_validation :set_success

  def set_success
    self.success = (amount == payment_request&.amount)
  end

  def finish_booking!
    return unless success

    booking.pay!
  end
end
