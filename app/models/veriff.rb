class Veriff
  API_TOKEN = ENV["VERIFF_API_TOKEN"]
  API_SECRET = ENV["VERIFF_API_SECRET"]
  API_URL = ENV["VERIFF_API_URL"]
  attr_accessor :user


  def self.is_signature_valid?(origin_signature, payload)
    payload = payload.to_json unless payload.is_a?(String)

    signature = OpenSSL::Digest::SHA256.new
    signature << payload
    signature << API_SECRET
    return signature.hexdigest == origin_signature
  end

  # code: (one of 9001, 9102, 9103, 9104, 9151, 9161)
  # Verification response code Explanation of the meaning of the response codes
  # 9001: Positive: Person was verified. The verification process is complete. Accessing the sessionURL again will show the client that nothing is to be done here.
  # 9102:Negative: Person has not been verified. The verification process is complete. Either it was a fraud case or some other severe reason that the person can not be verified. You should investigate the session further and read the "reason". If you decide to give the client another try you need to create a new session.
  # 9103:Resubmitted: Resubmission has been requested. The verification process is not completed. Something was missing from the client and she or he needs to go through the flow once more. The same sessionURL can and should be used for this purpose.
  # 9104:Negative: Verification has been expired. The verification process is complete. After 7 days the session get's expired. If the client started the verification process we reply "abandoned" here, otherwise if the client never arrived in our environment the status will be "expired"
  # 9151: Intermediate Positive: SelfID was successful - this code is only send if the configuration flag is set
  # 9161: Intermediate Positive: Video Call was successful - this code is only send if the configuration flag is set
  def self.update_user_status(params)
    session_id = params[:verification][:id]
    if user = User.where("veriff_data.id"=>session_id).first
      status= case params[:verification][:code]
              when 9001,9151,9161 then 'verified'
              when 9102,9103,9104 then 'unverified'
              else 'unverified'
              end
      user.aasm_state = status
      user.save!
    end
  end

  def initialize(user)
    @user = user
  end

  def start_verification
    payload = {
        verification: {
            person: {
                firstName: user.first_name,
                lastName: user.last_name,
            },
            lang: 'en',
            features: ['selfid'],
            timestamp: Time.now.utc.iso8601,
            callback: Rails.application.routes.url_helpers.root_url(Rails.configuration.action_mailer.default_url_options),
        }
    }


    http = Curl.post(API_URL+"/sessions", JSON.dump(payload)) do |http|
      http.headers.merge! headers(payload)
    end
    json = JSON.parse(http.body_str)
    user.update(veriff_data: {
        "id"     => json["verification"]["id"],
        "url"    => json["verification"]["url"],
        "status" =>json["verification"]["status"],
    })
  rescue => err
    p('verify method error', err);
  end

  # not verified response: {"status"=>"success", "verification"=>nil}
  # Not using as not supported by Veriff side
  def check_status
    session_id = user.veriff_data["id"]
    http = Curl.get("#{API_URL}/sessions/#{session_id}/decision") do |http|
      http.headers.merge! headers(session_id)
    end
    JSON.parse(http.body_str)
  rescue => err
    p('verify method error', err);
  end

  def headers(payload)
    {
        'x-auth-client': API_TOKEN,
        'x-signature': generateSignature(payload, API_SECRET),
        'content-type':'application/json'
    }
  end

  def generateSignature(payload, secret)
    payload = JSON.dump(payload) if payload.kind_of?(Hash)

    signature = OpenSSL::Digest::SHA256.new
    signature << payload
    signature << secret
    signature.hexdigest
  end
end