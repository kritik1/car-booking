class WebController < ActionController::Base
  include ActionView::Helpers::AssetUrlHelper
  include ActionView::Helpers::AssetTagHelper
  include Webpacker::Helper
  helper Webpacker::Helper

  def index
    render html: %Q{
      <!DOCTYPE html>
      <html>
        <head>
          <title>Car Booking</title>
          <meta content="initial-scale=1, maximum-scale=1" name="viewport" />
          #{stylesheet_pack_tag 'application'}
          #{javascript_pack_tag 'application'}
          </head>
          <body data="{}">
          <div id="main-app"></div>
        </body>
      </html>
    }.html_safe
  end
end