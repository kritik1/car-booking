class ApplicationController < ActionController::API
  def render_resources(array)
    had_errors = false
    new_arr = array.map do |resource|
      if resource.errors.empty?
        resource.as_json
      else
        had_errors = true
        validation_error(resource)
      end
    end

    render json: new_arr, status: (had_errors ? :bad_request : :ok)
  end

  def render_resource(resource)
    if resource.errors.empty?
      render json: resource
    else
      render json: validation_error(resource), status: :bad_request
    end
  end

  def validation_error(resource)
    {errors: [
        {
            status: '400',
            title: 'Bad Request',
            detail: resource.errors,
            code: '100'
        }
    ]}
  end

  def current_user
    return @current_user if defined?(@current_user)

    if token=request.headers['Authorization'].presence
      @current_user = User.find_by_token(token[7..-1])
    end
    @current_user
  end

  def check_session
    unless current_user
      render json: nil, status: :unauthorized and return
    end
  end
end
