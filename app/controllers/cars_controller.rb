class CarsController < ApplicationController
  before_action :check_session
  def search
    scope = Car.all
    if (starts_at = params.dig(:filter, :dates, :starts_at).presence) && (ends_at = params.dig(:filter, :dates, :ends_at))
      scope = scope.available_for(Time.at(starts_at.to_i).to_datetime, Time.at(ends_at.to_i).to_datetime)
    else
      scope = Car.none
    end

    render_resources(scope)
  end
end