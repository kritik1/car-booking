class WebhooksController < ApplicationController
  def receive
    if params[:provider]
      receive_veriff
    else
      render "Provider not known"
    end
  end

  private
  def receive_veriff
    signature = request.headers["x-signature"]

    if Veriff.is_signature_valid?(signature, request.body.read)
      Veriff.update_user_status(params)
      render json: {state: "ok"}, status: 200
    else
      render json: {state: "not ok"}, status: 404
    end
  end
end