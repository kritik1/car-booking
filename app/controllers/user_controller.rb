class UserController < ApplicationController
  before_action :check_session
  def show
    render_resource(current_user)
  end

  def ask_verification
    Veriff.new(current_user).start_verification
    render_resource(current_user)
  end

end