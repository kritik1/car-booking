class SessionsController < Devise::SessionsController
  after_action :record_action
  respond_to :json

  private

  def respond_with(resource, _opts = {})
    response.headers["Authorization"] = resource.jwt_token
    render json: resource
  end

  def respond_to_on_destroy
    head :no_content
  end

  def record_action
    UserLogin.log!(request, resource)
  end
end