class BookingsController < ApplicationController
  before_action :check_session, except: [:show, :pay]

  def index
    render_resources(Booking.where(user_id: current_user))
  end

  def create
    booking = Booking.new(user_id: current_user)
    strong_params = current_user.admin? ? params[:booking].permit! : user_params
    booking.attributes = strong_params
    booking.save
    render_resource(booking)
  end

  def show
    render_resource(load_booking)
  end

  def pay
    booking = load_booking
    request = PaymentRequest.create_attempt(booking, params[:payment][:id])

    if request.payment_method.auto_apply
      resp = PaymentResponse.create!(
          method_name:   request.method_name,
          amount:        request.amount,
          booking:       request.booking,
          payment_method:request.payment_method,
          payment_request: request
      )
      resp.finish_booking!
    end

    render_resource(booking.reload)
  end

  private
  def user_params
    params.require(:booking).permit(:car_id, :starts_at, :ends_at)
  end

  def load_booking
    @booking ||= Booking.find_by(token: params[:id])
  end
end